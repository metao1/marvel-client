package com.metao.marvel.common.adapter;


import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.metao.marvel.R;
import com.metao.marvel.data.model.Comic;
import com.metao.marvel.databinding.ComicListItemBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter for the comic list items.
 */
public class ComicListAdapter extends RecyclerView.Adapter<ComicListItemViewHolder> {

    private final ItemClickListener itemClickListener;
    private List<Comic> comicList;
    public ComicListAdapter(ItemClickListener itemClickListener) {
        comicList = new ArrayList<>();
        this.itemClickListener = itemClickListener;
    }

    public void setComicList(List<Comic> comicList) {
        this.comicList = comicList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ComicListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ComicListItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.item_comic_list, parent, false);
        return new ComicListItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ComicListItemViewHolder holder, int position) {
        final Comic comic = comicList.get(position);
        holder.bind(comic, itemClickListener);
    }

    @Override
    public int getItemCount() {
        return comicList.size();
    }

    public interface ItemClickListener {
        void onItemClicked(Comic comic);
    }
}
