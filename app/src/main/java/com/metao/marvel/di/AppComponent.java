package com.metao.marvel.di;

import com.metao.marvel.data.di.ApiModule;
import com.metao.marvel.data.di.ComicRepositoryModule;
import com.metao.marvel.presentation.bottomview.di.BottomViewComponent;
import com.metao.marvel.presentation.bottomview.di.BottomViewModule;
import com.metao.marvel.presentation.comicdetails.di.ComicDetailsComponent;
import com.metao.marvel.presentation.comicdetails.di.ComicDetailsPresenterModule;
import com.metao.marvel.presentation.comiclist.di.ComicListComponent;
import com.metao.marvel.presentation.comiclist.di.ComicListPresenterModule;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {ComicRepositoryModule.class, ApiModule.class, AppModule.class})
public interface AppComponent {

    ComicDetailsComponent plus(ComicDetailsPresenterModule module);

    ComicListComponent plus(ComicListPresenterModule module);

    BottomViewComponent plus(BottomViewModule module);


}
