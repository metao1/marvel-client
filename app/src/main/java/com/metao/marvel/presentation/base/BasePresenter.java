package com.metao.marvel.presentation.base;


/**
 * Base presenter.
 *
 * @param <V> - View
 */
public interface BasePresenter<V> {

    void onAttach(V view);

    void onDetach();

}
