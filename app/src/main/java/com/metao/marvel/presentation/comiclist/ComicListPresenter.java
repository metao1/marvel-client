package com.metao.marvel.presentation.comiclist;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.metao.marvel.common.scheduler.Job;
import com.metao.marvel.common.scheduler.SchedulerProvider;
import com.metao.marvel.data.ComicRepository;
import com.metao.marvel.data.model.Comic;
import com.metao.marvel.util.ErrorStringMapper;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Presenter implementation for the comic list.
 */
public class ComicListPresenter implements ComicListContract.Presenter {

    private static final String GET_KEY = "FETCH_CHARACTERS";
    private static final String JOB = "JOB";
    private final ComicRepository comicRepository;
    private final ErrorStringMapper errorStringMapper;
    private final SchedulerProvider schedulerProvider;
    private final ServiceHandler serviceHandler = new ServiceHandler(this);
    private CompositeDisposable compositeDisposable;
    private ComicListContract.View view;

    /**
     * Construct new presenter.
     *
     * @param comicRepository   - comic repository
     * @param errorStringMapper - error string mapper
     * @param schedulerProvider - scheduler provider
     */
    @Inject
    public ComicListPresenter(ComicRepository comicRepository,
                              ErrorStringMapper errorStringMapper,
                              SchedulerProvider schedulerProvider) {
        this.comicRepository = comicRepository;
        this.errorStringMapper = errorStringMapper;
        this.schedulerProvider = schedulerProvider;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onAttach(ComicListContract.View view) {
        this.view = view;
    }

    @Override
    public void onDetach() {
        compositeDisposable.clear();
    }

    @Override
    public void navigateToComicDetails(Comic comic) {
        view.showComicDetails(comic.getId());
    }

    @Override
    public void refreshComicList() {
        comicRepository.refresh();
        view.setRefreshing(true);
        fetchComics();
    }

    @Override
    public void fetchComicList(boolean showLoading) {
//        view.setLoading(showLoading);
        fetchComics();
    }

    private void fetchComics() {
        Job job = new Job(GET_KEY);
        Message message = new Message();
        Bundle bundle = new Bundle();
        bundle.putString(GET_KEY, GET_KEY);
        bundle.putParcelable(JOB, job);
        message.setData(bundle);
        serviceHandler.dispatchMessage(message);
    }

    private Runnable serviceControllerRunnable() {
        return () -> {
            Disposable disposable = comicRepository.getComics()
                    .observeOn(schedulerProvider.mainThread())
                    .subscribeOn(schedulerProvider.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(comicList -> {
                        // on next
                        view.setRefreshing(false);
                        view.setLoading(false);
                        view.showComicList(comicList);
                    }, throwable -> {
                        // on error
                        view.setLoading(false);
                        view.setRefreshing(false);
                        view.showLoadingError(errorStringMapper.getErrorMessage(throwable));
                    });
            compositeDisposable.add(disposable);
        };
    }

    public final static class ServiceHandler extends Handler {

        final WeakReference<ComicListPresenter> comicListPresenterWeakReference;

        ServiceHandler(ComicListPresenter comicListPresenter) {
            comicListPresenterWeakReference = new WeakReference<>(comicListPresenter);
        }

        @Override
        public void dispatchMessage(Message msg) {
            Bundle data = msg.getData();
            new Thread(comicListPresenterWeakReference.get().serviceControllerRunnable()).start();
            /*String dataString = data.getString(GET_KEY);
            if (dataString != null) {
                switch (dataString) {
                    case GET_KEY:
                        new Thread(comicListPresenterWeakReference.get().serviceControllerRunnable()).start();
                        break;
                }
            }*/
        }
    }
}
