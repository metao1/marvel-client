package com.metao.marvel.presentation.bottomview.di;


import com.metao.marvel.data.di.ScreenScoped;
import com.metao.marvel.presentation.bottomview.BottomViewContract;

import dagger.Module;
import dagger.Provides;

/**
 * Comic list module.
 */
@Module
public class BottomViewModule {

    private final BottomViewContract.View view;

    public BottomViewModule(BottomViewContract.View view) {
        this.view = view;
    }

    @Provides
    @ScreenScoped
    BottomViewContract.View provideView() {
        return view;
    }

}
