package com.metao.marvel.presentation.comiclist;

public interface ItemsSwitchListener {
    void onItemChanged(String itemId);
}