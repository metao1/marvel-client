package com.metao.marvel.presentation.comiclist;


import android.databinding.DataBindingUtil;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.metao.marvel.App;
import com.metao.marvel.R;
import com.metao.marvel.common.adapter.ComicListAdapter;
import com.metao.marvel.data.model.Comic;
import com.metao.marvel.databinding.ComicListBinding;
import com.metao.marvel.presentation.base.BaseFragment;
import com.metao.marvel.presentation.comicdetails.ComicDetailsFragment;
import com.metao.marvel.presentation.comiclist.di.ComicListPresenterModule;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;

import javax.inject.Inject;

/**
 * Fragment for the Comic List.
 */
public class ComicListFragment extends BaseFragment implements ComicListContract.View,
        ComicListAdapter.ItemClickListener {

    private static final int LIKE = 4;
    private static final int DISLIKE = 8;
    @Inject
    ComicListPresenter presenter;
    private ComicListBinding binding;
    private ComicListAdapter adapter;
    private Queue<Integer> recoverQueue;

    private RecyclerItemTouchHelperListener listener = (viewHolder, direction, position) -> {
        Toast.makeText(getContext(), String.format(Locale.US, "You %s it!", direction == LIKE ? "liked" : "disliked!"),
                Toast.LENGTH_SHORT).show();
        if (direction == LIKE) {

        } else if (direction == DISLIKE) {

        }
        recoverQueue.add(position);
        recoverSwipedItem();

    };

    public static Fragment newInstance() {
        return new ComicListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        injectComponents();
        presenter.onAttach(this);
        recoverQueue = new LinkedList<Integer>() {
            @Override
            public boolean add(Integer o) {
                return !contains(o) && super.add(o);
            }
        };
    }

    private void injectComponents() {
        if (getActivity() != null) {
            ((App) getActivity().getApplication())
                    .getAppComponent()
                    .plus(new ComicListPresenterModule(this))
                    .inject(this);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_comic_list, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupRecyclerView();
        presenter.fetchComicList(true);
    }

    @Override
    public void onDestroy() {
        presenter.onDetach();
        super.onDestroy();
    }

    private void setupRecyclerView() {
        if (adapter == null) {
            adapter = new ComicListAdapter(this);
        }
        binding.comicListRecycle.setAdapter(adapter);
        binding.comicListRecycle.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.refresh.setOnRefreshListener(() -> presenter.refreshComicList());
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                listener.onSwiped(viewHolder, direction, viewHolder.getAdapterPosition());
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }

            @Override
            public void onChildDrawOver(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                super.onChildDrawOver(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(binding.comicListRecycle);
    }

    @Override
    public void setLoading(boolean loading) {
        binding.loadingProgress.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setRefreshing(boolean refreshing) {
        binding.refresh.setRefreshing(refreshing);
    }

    @Override
    public void showComicDetails(long comicId) {
        startFragment(ComicDetailsFragment.newInstance(comicId));
    }

    @Override
    public void showComicList(List<Comic> comicList) {
        adapter.setComicList(comicList);
    }

    @Override
    public void showLoadingError(String error) {
        Snackbar.make(binding.comicListRecycle, error, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.error_action_retry,
                        view -> presenter.fetchComicList(true)).show();
    }

    private synchronized void recoverSwipedItem() {
        while (!recoverQueue.isEmpty()) {
            int pos = recoverQueue.poll();
            if (pos > -1) {
                binding.comicListRecycle.getAdapter().notifyItemChanged(pos);
            }
        }
    }

    @Override
    public void onItemClicked(Comic comic) {
        presenter.navigateToComicDetails(comic);
    }

    public interface RecyclerItemTouchHelperListener {
        void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);
    }
}
