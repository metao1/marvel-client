package com.metao.marvel.presentation.comiclist;


import com.metao.marvel.data.model.Comic;
import com.metao.marvel.presentation.base.BasePresenter;
import com.metao.marvel.presentation.base.BaseView;

import java.util.List;

/**
 * Contract between the View and Presenter.
 */
public interface ComicListContract {

    interface Presenter extends BasePresenter<View> {

        void navigateToComicDetails(Comic comic);

        void refreshComicList();

        void fetchComicList(boolean showLoading);

    }

    interface View extends BaseView {

        void setLoading(boolean loading);

        void setRefreshing(boolean refreshing);

        void showComicDetails(long comicId);

        void showComicList(List<Comic> comicList);

        void showLoadingError(String error);

    }

}
