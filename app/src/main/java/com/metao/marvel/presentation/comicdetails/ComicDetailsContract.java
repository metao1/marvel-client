package com.metao.marvel.presentation.comicdetails;


import com.metao.marvel.data.model.ComicCreators;
import com.metao.marvel.data.model.ComicPrice;
import com.metao.marvel.presentation.base.BasePresenter;
import com.metao.marvel.presentation.base.BaseView;

import java.util.List;

/**
 * Contract between the view and presenter of the Comic details screen.
 */
public interface ComicDetailsContract {

    interface Presenter extends BasePresenter<View> {

        void fetchComicDetails(long comicId);

    }

    interface View extends BaseView {

        void showTitle(String title);

        void showDescription(String description);

        void showPageCount(String pageCount);

        void showPrices(List<ComicPrice> prices);

        void showAuthors(ComicCreators authors);

        void showComicImage(String imageUrl);

        void showLoading(boolean isLoading);

        void showError(String error);

    }

}
