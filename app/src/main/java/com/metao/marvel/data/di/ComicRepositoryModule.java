package com.metao.marvel.data.di;


import com.metao.marvel.data.ComicDataPersistence;
import com.metao.marvel.data.ComicDataSource;
import com.metao.marvel.data.local.LocalComicDataPersistence;
import com.metao.marvel.data.local.LocalComicDataSource;
import com.metao.marvel.data.local.mappers.ComicRealmMapper;
import com.metao.marvel.data.remote.ApiService;
import com.metao.marvel.data.remote.RemoteComicDataSource;
import com.metao.marvel.data.remote.TimeStampProvider;
import com.metao.marvel.util.HashCalculator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Module for providing comic repository.
 */
@Module
public class ComicRepositoryModule {

    @Provides
    @Singleton
    @Remote
    ComicDataSource provideRemoteComicDataSource(ApiService apiService,
                                                 HashCalculator hashCalculator,
                                                 TimeStampProvider timeStampProvider) {
        return new RemoteComicDataSource(apiService, hashCalculator, timeStampProvider);
    }

    @Provides
    @Singleton
    @Local
    ComicDataSource provideLocalComicDataSource(ComicRealmMapper mapper) {
        return new LocalComicDataSource(mapper);
    }

    @Provides
    @Singleton
    @Local
    ComicDataPersistence provideLocalComicDataPersistence() {
        return new LocalComicDataPersistence();
    }

}
