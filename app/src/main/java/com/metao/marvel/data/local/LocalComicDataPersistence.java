package com.metao.marvel.data.local;


import android.support.annotation.NonNull;

import com.metao.marvel.data.ComicDataPersistence;
import com.metao.marvel.data.local.realm.ComicRealm;
import com.metao.marvel.data.local.realm.ImageRealm;
import com.metao.marvel.data.local.realm.Tables;
import com.metao.marvel.data.model.Comic;
import com.metao.marvel.data.model.Image;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Writes comic data into local db (using realm).
 */
@Singleton
public class LocalComicDataPersistence implements ComicDataPersistence {

    @Inject
    public LocalComicDataPersistence() {
    }

    @Override
    public void save(@NonNull final Comic comic) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                findOrCreateComic(realm, comic);
            }
        });
    }

    public void update(@NonNull final ComicRealm comicRealm) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                updateComic(realm, comicRealm);
            }
        });
    }

    private void updateComic(Realm realm, ComicRealm comic) {
        ComicRealm foundComicRealm = realm.where(ComicRealm.class)
                .equalTo("id", comic.getId())
                .findFirst();
        if (foundComicRealm != null) {
            realm.insertOrUpdate(foundComicRealm);
        }
    }

    private void findOrCreateComic(Realm realm, Comic comic) {
        ComicRealm comicRealm = realm.where(ComicRealm.class)
                .equalTo("id", comic.getId())
                .findFirst();

        if (comicRealm == null) {
            comicRealm = realm.createObject(ComicRealm.class, comic.getId());
            comicRealm.setTitle(comic.getTitle());
            comicRealm.setLiked(false);
            comicRealm.setDescription(comic.getDescription());
            comicRealm.setPageCount(comic.getPageCount());
            comicRealm.setThumbnail(findOrCreateImage(realm, comic.getThumbnail()));
            comicRealm.setImages(findOrCreateImages(realm, comic.getImages()));
        }
    }

    private ImageRealm findOrCreateImage(Realm realm, Image image) {
        ImageRealm imageRealm = realm.where(ImageRealm.class)
                .equalTo(Tables.Image.path, image.getPath())
                .equalTo(Tables.Image.extension, image.getExtension())
                .findFirst();

        if (imageRealm == null) {
            imageRealm = realm.createObject(ImageRealm.class, image.getPath());

            imageRealm.setExtension(image.getExtension());
        }
        return imageRealm;
    }

    private RealmList<ImageRealm> findOrCreateImages(Realm realm, List<Image> images) {
        RealmList<ImageRealm> imageRealms = new RealmList<>();
        if (images != null) {
            for (Image image : images) {
                imageRealms.add(findOrCreateImage(realm, image));
            }
        }
        return imageRealms;
    }
}
