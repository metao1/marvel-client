package com.metao.marvel.data;


import com.metao.marvel.data.model.Comic;

import java.util.List;

import io.reactivex.Observable;

/**
 * Interface for Comic data sources.
 */
public interface ComicDataSource {

    Observable<List<Comic>> getComics();

    Observable<Comic> getComic(long comicId);

}
