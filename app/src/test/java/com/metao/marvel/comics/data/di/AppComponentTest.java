package com.metao.marvel.comics.data.di;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApiModuleTest.class})
public interface AppComponentTest {

    BottomViewModuleTest plus(BottomViewModuleTest bottomViewModuleTest);
}
